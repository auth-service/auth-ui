export const UPDATE_INPUT = 'UPDATE_INPUT';
export const UPDATE_INPUT_VALIDATION = 'UPDATE_INPUT_VALIDATION';
export const RESET_INPUTS = 'RESET_INPUTS';

const initialState = {
  resetPassword: {
    password: {
      value: '',
      validation: {
        display: false,
        details: '',
        type: '',
      },
    },
    password2: {
      value: '',
      validation: {
        display: false,
        details: '',
        type: '',
      },
    },
  },
  registration: {
    email: {
      value: '',
      validation: {
        display: false,
        details: '',
        type: '',
      },
    },
    username: {
      value: '',
      validation: {
        display: false,
        details: '',
        type: '',
      },
    },
    password: {
      value: '',
      validation: {
        display: false,
        details: '',
        type: '',
      },
    },
    password2: {
      value: '',
      validation: {
        display: false,
        details: '',
        type: '',
      },
    },
  },
  forgotPass: {
    email: {
      value: '',
      validation: {
        display: false,
        details: '',
        type: '',
      },
    },
  },
};

const Ui = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_INPUT:
      return {
        ...state,
        [action.data.page]: {
          ...state[action.data.page],
          [action.data.key]: {
            value: action.data.value,
            validation: state[action.data.page][action.data.key].validation,
          },
        },
      };
    case UPDATE_INPUT_VALIDATION:
      return {
        ...state,
        [action.data.page]: {
          ...state[action.data.page],
          [action.data.key]: {
            ...state[action.data.page][action.data.key],
            validation: action.data.value,
          },
        },
      };
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};

export default Ui;
