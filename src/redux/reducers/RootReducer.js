import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import Ui from './Ui.reducer';
import Auth from './Auth.reducer';
import Inputs from './Inputs.reducer';

const rootReducer = history => combineReducers({
  Ui,
  Auth,
  Inputs,
  router: connectRouter(history),
});

export default rootReducer;
