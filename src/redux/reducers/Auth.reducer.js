export const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE';
export const SET_BEARER_TOKEN = 'SET_BEARER_TOKEN';
export const SET_USER_DATA = 'SET_USER_DATA';

const initialState = {
  bearerToken: localStorage.getItem('bearerToken') || '',
  userData: null,
  error: {
    display: false,
    message: '',
    title: '',
  },
};

const Auth = (state = initialState, action) => {
  switch (action.type) {
    case SET_BEARER_TOKEN:
      return {
        ...state,
        bearerToken: action.data,
      };
    case SET_USER_DATA:
      return {
        ...state,
        userData: action.data,
      };
    case SET_ERROR_MESSAGE:
      return {
        ...state,
        error: action.data,
      };
    case 'RESET_STATE':
      return initialState;
    default:
      return state;
  }
};

export default Auth;
