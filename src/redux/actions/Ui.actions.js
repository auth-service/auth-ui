import api from '../../assets/utilities/api';

export default {
  async resendLink(payload) {
    const result = await api.post(`${process.env.REACT_APP_AUTH_URL}/verify`, payload);
    return result;
  },
};
