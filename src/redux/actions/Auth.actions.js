import api from '../../assets/utilities/api';

export default {
  /**
   * Login with the provided user
   *
   * @returns {object}
   */
  async login(payload) {
    const result = await api.post(`${process.env.REACT_APP_AUTH_URL}/sessions`, payload);
    return result;
  },

  /**
   * Creates a new user
   *
   * @returns {object}
   */
  async register(payload) {
    const result = await api.post(`${process.env.REACT_APP_AUTH_URL}/users`, payload);
    return result;
  },

  /**
   * Forgot password
   *
   * @returns {object}
   */
  async forgotPassword(payload) {
    const result = await api.post(`${process.env.REACT_APP_AUTH_URL}/password-reset`, payload);
    return result;
  },

  /**
   * Resets user password
   *
   * @returns {object}
   */
  async resetUserPassword(payload, sessionId) {
    const result = await api.patch(`${process.env.REACT_APP_AUTH_URL}/password-reset/${sessionId}`, payload);
    return result;
  },
};
