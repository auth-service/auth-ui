import AuthActions from '../actions/Auth.actions';
import { setCookie } from '../../assets/utilities/cookies';

import { SET_BEARER_TOKEN, SET_ERROR_MESSAGE, SET_USER_DATA } from '../reducers/Auth.reducer';
import { UPDATE_INPUT_VALIDATION } from '../reducers/Inputs.reducer';

// Logs a user in
export const login = (loginDetail, password, rememberMe, returnAdd) => async (dispatch) => {
  const payload = {
    login: loginDetail,
    password: password,
    thirtyDay: rememberMe,
  };

  const res = await AuthActions.login(payload);

  // If an error occoured
  if ('errors' in res) {
    return (
      dispatch({
        type: SET_ERROR_MESSAGE,
        data: {
          display: true,
          title: res.errors[0].title,
          message: res.errors[0].detail,
        },
      })
    );
  }

  // Logged in sucessfully
  setCookie('authData', res.data.token);

  window.location.href = returnAdd;

  return null;
};

// Registers a user
export const createUser = (
  inEmail,
  inUsername,
  inPassword,
  inPassword2,
  returnAdd,
) => async (dispatch) => {
  const payload = {
    userData: {
      email: inEmail,
      username: inUsername,
      password: inPassword,
    },
    returnAdd: returnAdd,
  };

  const res = await AuthActions.register(payload);

  // If an error occoured
  if ('errors' in res) {
    const errors = [];
    let data;

    res.errors.forEach((element) => {
      // Display input errors
      if ('meta' in element) {
        data = {
          type: UPDATE_INPUT_VALIDATION,
          data: {
            key: `${element.meta.inputRef}`,
            page: 'registration',
            value: {
              display: true,
              details: element.detail,
              type: 'error',
            },
          },
        };
      // Display alert error
      } else {
        data = {
          type: SET_ERROR_MESSAGE,
          data: {
            display: true,
            message: element.detail,
            title: element.title,
          },
        };
      }
      errors.push(data);
    });

    return (
      dispatch(errors)
    );
  }

  setCookie('authData', res.data.token);

  dispatch([
    {
      type: SET_BEARER_TOKEN,
      data: res.data.token,
    },
    {
      type: SET_USER_DATA,
      data: res.data.userInfo,
    },
  ]);

  window.location.href = returnAdd;
  return null;
};

// Closes the error message
export const closeErrorMessage = () => dispatch => (
  dispatch({
    type: SET_ERROR_MESSAGE,
    data: {
      display: false,
      title: '',
      message: '',
    },
  })
);

export const requestPasswordReset = email => async (dispatch) => {
  if (email === '') {
    return dispatch({
      type: SET_ERROR_MESSAGE,
      data: {
        display: true,
        title: 'Error',
        message: 'Please enter an email',
      },
    });
  }

  const payload = {
    email: email,
  };

  const res = await AuthActions.forgotPassword(payload);

  console.log(res);

  return dispatch({
    type: '',
    data: {},
  });
};

// Resets a users password
export const resetUserPassword = (password, sessionId) => async (dispatch) => {
  const payload = {
    password: password,
  };

  const res = await AuthActions.resetUserPassword(payload, sessionId);

  console.log(res);

  return dispatch({
    type: '',
    data: {},
  });
};
