import {
  UPDATE_INPUT,
} from '../reducers/Inputs.reducer';

export const updateInput = (page, key, value) => ({
  type: UPDATE_INPUT,
  data: {
    page: page,
    key: key,
    value: value,
  },
});
