import React from 'react';
import PropTypes from 'prop-types';

const CheckBoxInput = (props) => {
  const {
    id,
    value,
    updateValue,
    label,
  } = props;

  return (
    <div className="text-input-container">
      <div className="text-input">
        <label
          className="check-box"
          htmlFor="remember-me"
        >
          <input
            type="checkbox"
            id={id}
            checked={value}
            onChange={(e) => {
              updateValue(e.target.checked);
            }}
          />
          {label}
        </label>
      </div>
    </div>
  );
};

CheckBoxInput.propTypes = {
  id: PropTypes.string.isRequired,
  updateValue: PropTypes.func.isRequired,
  value: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
};


export default CheckBoxInput;
