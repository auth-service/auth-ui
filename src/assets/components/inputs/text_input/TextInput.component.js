import React, { useState } from 'react';
import PropTypes from 'prop-types';

import InputMessage from './input_message/InputMessage.component';

const TextInput = (props) => {
  const {
    id,
    type,
    placeholder,
    value,
    messageDisplay,
    message,
    messageType,
    updateValue,
  } = props;

  const [messDisplay, setMessDisplay] = useState(false);

  const inputMessage = messDisplay ? (
    <InputMessage
      display={messageDisplay}
      message={message}
      messageType={messageType}
    />
  ) : null;

  return (
    <div className="text-input-container">
      <input
        id={id}
        className={`input text-input ${messageType}`}
        placeholder={placeholder}
        type={type}
        value={value}
        onChange={(e) => { updateValue(e.target.value); }}
        onBlur={() => { setMessDisplay(false); }}
        onFocus={() => { setMessDisplay(true); }}
      />
      {inputMessage}
    </div>
  );
};

TextInput.defaultProps = {
  messageDisplay: false,
  message: '',
  messageType: '',
};

TextInput.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  updateValue: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  messageDisplay: PropTypes.bool,
  message: PropTypes.string,
  messageType: PropTypes.string,
};


export default TextInput;
