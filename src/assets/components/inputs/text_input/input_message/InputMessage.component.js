import React from 'react';
import PropTypes from 'prop-types';

import { faExclamation } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const InputMessage = ({ display, message, messageType }) => (
  <div className="input-message-container">
    {
      display ? (
        <div className="input-message">
          <div className={`icon-wrapper ${messageType}`}>
            <FontAwesomeIcon
              icon={faExclamation}
            />
          </div>
          <div className="text">
            {message}
          </div>
        </div>
      ) : null
    }
  </div>
);

InputMessage.propTypes = {
  display: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  messageType: PropTypes.string.isRequired,
};

export default InputMessage;
