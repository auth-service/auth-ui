import { connect } from 'react-redux';
import InfoBanner from './InfoBanner.component';

import { setInfoBanner } from '../../../redux/action_creators/Ui.actioncreator';

const mapStateToProps = state => ({
  display: state.Ui.infoBanner.display,
  message: state.Ui.infoBanner.message,
  type: state.Ui.infoBanner.type,
});

const mapDispatchToProps = dispatch => ({
  close: () => dispatch(setInfoBanner(false, '', {})),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InfoBanner);
