import {
  UPDATE_INPUT_VALIDATION,
} from '../../../redux/reducers/Inputs.reducer';

import testEmail from './tests/EmailTest';

export const verifyEmail = (store, email) => {
  if (email === '') {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'forgotPass',
          key: 'email',
          value: {
            display: true,
            details: 'The email field is required.',
            type: 'error',
          },
        },
      })
    );
  }

  if (!testEmail(email)) {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'forgotPass',
          key: 'email',
          value: {
            display: true,
            details: 'Valid email must be entered.',
            type: 'error',
          },
        },
      })
    );
  }

  return (
    store.dispatch({
      type: UPDATE_INPUT_VALIDATION,
      data: {
        page: 'forgotPass',
        key: 'email',
        value: {
          display: false,
          details: '',
          type: '',
        },
      },
    })
  );
};

const forgotPassValidation = (store, action) => {
  switch (action.data.key) {
    case 'email':
      verifyEmail(store, action.data.value);
      break;
    default:
      break;
  }
};

export default forgotPassValidation;
