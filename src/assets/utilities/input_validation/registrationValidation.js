import {
  UPDATE_INPUT_VALIDATION,
} from '../../../redux/reducers/Inputs.reducer';

import testEmail from './tests/EmailTest';
import testPassword from './tests/PasswordTest';

export const verifyEmail = (store, email) => {
  if (email === '') {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'registration',
          key: 'email',
          value: {
            display: true,
            details: 'The email field is required.',
            type: 'error',
          },
        },
      })
    );
  }

  if (!testEmail(email)) {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'registration',
          key: 'email',
          value: {
            display: true,
            details: 'Valid email must be entered.',
            type: 'error',
          },
        },
      })
    );
  }

  return (
    store.dispatch({
      type: UPDATE_INPUT_VALIDATION,
      data: {
        page: 'registration',
        key: 'email',
        value: {
          display: false,
          details: '',
          type: '',
        },
      },
    })
  );
};

const verifyUsername = (store, username) => {
  if (username === '') {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'registration',
          key: 'username',
          value: {
            display: true,
            details: 'The username field is required.',
            type: 'error',
          },
        },
      })
    );
  }

  return (
    store.dispatch({
      type: UPDATE_INPUT_VALIDATION,
      data: {
        page: 'registration',
        key: 'username',
        value: {
          display: false,
          details: '',
          type: '',
        },
      },
    })
  );
};

const verifyPassword = (store, password) => {
  const regData = store.getState().Inputs.registration;

  if (password === '') {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'registration',
          key: 'password',
          value: {
            display: false,
            details: '',
            type: '',
          },
        },
      })
    );
  }

  // If the email and password are the same.
  if (regData.email.value === password) {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'registration',
          key: 'password',
          value: {
            display: true,
            details: 'Email and Password cannot be the same.',
            type: 'error',
          },
        },
      })
    );
  }

  // If the username and password are the same.
  if (regData.username === password) {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'registration',
          key: 'password',
          value: {
            display: true,
            details: 'Username and Password cannot be the same.',
            type: 'error',
          },
        },
      })
    );
  }

  const res = testPassword(password);

  return (
    store.dispatch({
      type: UPDATE_INPUT_VALIDATION,
      data: {
        page: 'registration',
        key: 'password',
        value: {
          display: true,
          details: res.details,
          type: res.type,
        },
      },
    })
  );
};

const verifyPassword2 = (store, password2) => {
  const password = store.getState().Inputs.registration.password.value;

  if (password2 === '') {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'registration',
          key: 'password2',
          value: {
            display: false,
            details: '',
            type: '',
          },
        },
      })
    );
  }

  // If the passwords do not match
  if (password !== password2) {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'registration',
          key: 'password2',
          value: {
            display: true,
            details: 'Passwords do not match.',
            type: 'error',
          },
        },
      })
    );
  }

  return (
    store.dispatch({
      type: UPDATE_INPUT_VALIDATION,
      data: {
        page: 'registration',
        key: 'password2',
        value: {
          display: false,
          details: '',
          type: '',
        },
      },
    })
  );
};

const registrationValidation = (store, action) => {
  switch (action.data.key) {
    case 'email':
      verifyEmail(store, action.data.value);
      break;
    case 'username':
      verifyUsername(store, action.data.value);
      break;
    case 'password':
      verifyPassword(store, action.data.value);
      break;
    case 'password2':
      verifyPassword2(store, action.data.value);
      break;
    default:
      break;
  }
};

export default registrationValidation;
