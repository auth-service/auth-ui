import testPassword from './tests/PasswordTest';

import {
  UPDATE_INPUT_VALIDATION,
} from '../../../redux/reducers/Inputs.reducer';

const verifyPassword = (store, password) => {
  if (password === '') {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'resetPassword',
          key: 'password',
          value: {
            display: false,
            details: '',
            type: '',
          },
        },
      })
    );
  }

  const res = testPassword(password);

  return (
    store.dispatch({
      type: UPDATE_INPUT_VALIDATION,
      data: {
        page: 'resetPassword',
        key: 'password',
        value: {
          display: true,
          details: res.details,
          type: res.type,
        },
      },
    })
  );
};

const verifyPassword2 = (store, password2) => {
  const password = store.getState().Inputs.resetPassword.password.value;

  if (password2 === '') {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'resetPassword',
          key: 'password2',
          value: {
            display: false,
            details: '',
            type: '',
          },
        },
      })
    );
  }

  // If the passwords do not match
  if (password !== password2) {
    return (
      store.dispatch({
        type: UPDATE_INPUT_VALIDATION,
        data: {
          page: 'resetPassword',
          key: 'password2',
          value: {
            display: true,
            details: 'Passwords do not match.',
            type: 'error',
          },
        },
      })
    );
  }

  return (
    store.dispatch({
      type: UPDATE_INPUT_VALIDATION,
      data: {
        page: 'resetPassword',
        key: 'password2',
        value: {
          display: false,
          details: '',
          type: '',
        },
      },
    })
  );
};

const resetPasswordValidation = (store, action) => {
  switch (action.data.key) {
    case 'password':
      verifyPassword(store, action.data.value);
      break;
    case 'password2':
      verifyPassword2(store, action.data.value);
      break;
    default:
      break;
  }
};

export default resetPasswordValidation;
