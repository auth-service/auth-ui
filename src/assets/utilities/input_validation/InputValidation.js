import {
  UPDATE_INPUT,
} from '../../../redux/reducers/Inputs.reducer';

import registrationValidation from './registrationValidation';
import resetPasswordValidation from './resetPasswordValidation';
import forgotPassValidation from './forgotPassValidation';

export const inputValidation = store => next => (action) => {
  if (action.type === UPDATE_INPUT) {
    switch (action.data.page) {
      case 'registration':
        registrationValidation(store, action);
        break;
      case 'resetPassword':
        resetPasswordValidation(store, action);
        break;
      case 'forgotPass':
        forgotPassValidation(store, action);
        break;
      default:
        break;
    }
  }

  next(action);
};

export default inputValidation;
