const testPassword = (password) => {
  const strongRegex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})');
  const mediumRegex = new RegExp('^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})');

  // Strong password
  if (strongRegex.test(password)) {
    return ({
      details: 'Strong password',
      type: 'confirm',
    });
  }

  if (mediumRegex.test(password)) {
    return ({
      details: 'Warning medium password',
      type: 'alert',
    });
  }

  return ({
    details: 'Error weak password',
    type: 'error',
  });
};

export default testPassword;
