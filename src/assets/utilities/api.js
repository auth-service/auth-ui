import store from '../../store';

import {
  SET_BEARER_TOKEN,
  SET_ERROR_MESSAGE,
} from '../../redux/reducers/Auth.reducer';

// Runs through errors returned to see if expired token is returned
const checkForInvalidTokenError = (json) => {
  if ('errors' in json) {
    json.errors.forEach((e) => {
      // Expired token error was found.
      if (e.code === 'a-e-000005') {
        // Deletes token in local storage.
        localStorage.removeItem('bearerToken');
        // Delete token from store.
        store.dispatch({
          type: SET_BEARER_TOKEN,
          data: '',
        });
        // Resets the state
        store.dispatch({ type: 'RESET_STATE' });
        // Display error
        store.dispatch({
          type: SET_ERROR_MESSAGE,
          data: {
            display: true,
            message: e.detail,
            title: e.title,
          },
        });
      }
    });
  }
};

export default {
  /**
   * Get the required headers for the request.
   *
   * @returns {headers}
   */
  getHeaders(token = '') {
    if (token !== '') {
      return new Headers({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      });
    }

    return new Headers({
      'Content-Type': 'application/json',
    });
  },

  /**
   * Make a GET request to the desired URL and return the response.
   *
   * @param {string} url
   * @returns {promise}
   */
  async get(url, token = null) {
    const json = await fetch(url, {
      method: 'GET',
      headers: this.getHeaders(token),
    })
      .then(response => response.json());

    await checkForInvalidTokenError(json);
    return json;
  },

  /**
   * Make a POST request to the desired URL and return the response.
   *
   * @param {string} url
   * @param {object} parameters
   * @returns {promise}
   */
  async post(url, parameters, token = null) {
    const json = await fetch(url, {
      method: 'POST',
      headers: this.getHeaders(token),
      body: JSON.stringify(parameters),
    })
      .then(response => response.json());

    await checkForInvalidTokenError(json);
    return json;
  },

  /**
   * Make a PUT request to the desired URL and return the response.
   *
   * @param {string} url
   * @param {object} parameters
   * @returns {promise}
   */
  async put(url, parameters, token = null) {
    const json = await fetch(url, {
      method: 'PUT',
      headers: this.getHeaders(token),
      body: JSON.stringify(parameters),
    })
      .then(response => response.json());

    await checkForInvalidTokenError(json);
    return json;
  },

  /**
   * Make a PATCH request to the desired URL and return the response.
   *
   * @param {string} url
   * @param {object} parameters
   * @returns {promise}
   */
  async patch(url, parameters, token = null) {
    const json = await fetch(url, {
      method: 'PATCH',
      headers: this.getHeaders(token),
      body: JSON.stringify(parameters),
    })
      .then(response => response.json());

    await checkForInvalidTokenError(json);
    return json;
  },

  /**
   * Make a DELETE request to the desired URL and return the response.
   *
   * @param {string} url
   * @returns {promise}
   */
  async delete(url, token = null) {
    const json = await fetch(url, {
      method: 'DELETE',
      headers: this.getHeaders(token),
    })
      .then(response => response.json());

    await checkForInvalidTokenError(json);
    return json;
  },
};
