import React from 'react';
import './assets/styles/styles.min.css';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';

import history from './assets/utilities/history';
import Auth from './pages/auth/Auth';
import ResetPassword from './pages/reset_password/ResetPassword';
import ForgotPassword from './pages/forgot_password/ForgotPassword';

const App = () => (
  <ConnectedRouter history={history}>
    <div>
      <Route
        path="/"
        component={Auth}
        exact
      />
      <Route
        path="/password-reset/:sessionId"
        component={ResetPassword}
      />
      <Route
        path="/forgot-password"
        component={ForgotPassword}
      />
    </div>
  </ConnectedRouter>
);

export default App;
