import { connect } from 'react-redux';
import { updateInput } from '../../../../redux/action_creators/Inputs.actioncreator';
import TextInput from '../../../../assets/components/inputs/text_input/TextInput.component';

const mapStateToProps = state => ({
  value: state.Inputs.forgotPass.email.value,
  messageDisplay: state.Inputs.forgotPass.email.validation.display,
  message: state.Inputs.forgotPass.email.validation.details,
  messageType: state.Inputs.forgotPass.email.validation.type,
});

const mapDispatchToProps = dispatch => ({
  updateValue: value => dispatch(
    updateInput('forgotPass', 'email', value),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextInput);
