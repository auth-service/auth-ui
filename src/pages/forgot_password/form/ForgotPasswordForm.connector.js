import { connect } from 'react-redux';
import { requestPasswordReset } from '../../../redux/action_creators/Auth.actioncreator';
import ForgotPasswordForm from './ForgotPasswordForm.component';

const mapStateToProps = state => ({
  email: state.Inputs.forgotPass.email.value,
});

const mapDispatchToProps = dispatch => ({
  reset: value => dispatch(
    requestPasswordReset(value),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPasswordForm);
