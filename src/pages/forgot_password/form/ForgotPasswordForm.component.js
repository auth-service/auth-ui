import React from 'react';
import PropTypes from 'prop-types';

import Email from './inputs/Email.connector';

const ResetPasswordForm = ({ reset, email }) => (
  <form>
    <Email
      id="email-log"
      className="input text-input"
      placeholder="Username or Email"
      type="text"
    />
    <input
      type="button"
      value="Submit"
      onClick={() => {
        reset(email);
      }}
    />
  </form>
);

ResetPasswordForm.propTypes = {
  reset: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
};

export default ResetPasswordForm;
