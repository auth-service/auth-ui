import React from 'react';

import ForgotPasswordForm from './form/ForgotPasswordForm.connector';

const ResetPassword = () => (
  <div id="divPageWrapper" className="reset-password-overlay">
    <div id="reset-password-window">
      <div className="reset-password-component-container">
        <div id="reset-password-card">
          <h3 className="title is-3 text-align-center">
            Reset Password
          </h3>
          <ForgotPasswordForm />
        </div>
      </div>
    </div>
  </div>
);

export default ResetPassword;
