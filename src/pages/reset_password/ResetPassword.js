import React from 'react';
import PropTypes from 'prop-types';

import ResetPasswordForm from './form/ResetPasswordForm.connector';

const ResetPassword = ({ match }) => ( 
  <div id="divPageWrapper" className="reset-password-overlay">
    <div id="reset-password-window">
      <div className="reset-password-component-container">
        <div id="reset-password-card">
          <h3 className="title is-3 text-align-center">
            Reset Password
          </h3>
          <ResetPasswordForm
            sessionId={match.params.sessionId}
          />
        </div>
      </div>
    </div>
  </div>
);

ResetPassword.propTypes = {
  match: PropTypes.object.isRequired,
};

export default ResetPassword;
