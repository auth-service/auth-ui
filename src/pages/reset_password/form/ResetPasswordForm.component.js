import React from 'react';
import PropTypes from 'prop-types';

import Password from './inputs/Password.connector';
import PasswordConfirmation from './inputs/PasswordConfirmation.connector';

const ResetPasswordForm = ({ reset, password, sessionId }) => (
  <form>
    <Password
      id="res-password"
      type="password"
      placeholder="Password"
    />
    <PasswordConfirmation
      id="res-password2"
      type="password"
      placeholder="Password Confirmation"
    />
    <input
      type="button"
      value="Submit"
      onClick={() => {
        reset(password, sessionId);
      }}
    />
  </form>
);

ResetPasswordForm.propTypes = {
  reset: PropTypes.func.isRequired,
  password: PropTypes.string.isRequired,
  sessionId: PropTypes.string.isRequired,
};

export default ResetPasswordForm;
