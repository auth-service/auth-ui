import { connect } from 'react-redux';
import { updateInput } from '../../../../redux/action_creators/Inputs.actioncreator';
import TextInput from '../../../../assets/components/inputs/text_input/TextInput.component';


const mapStateToProps = state => ({
  value: state.Inputs.resetPassword.password2.value,
  message: state.Inputs.resetPassword.password2.validation.details,
  messageDisplay: state.Inputs.resetPassword.password2.validation.display,
  messageType: state.Inputs.resetPassword.password2.validation.type,
});

const mapDispatchToProps = dispatch => ({
  updateValue: value => dispatch(
    updateInput('resetPassword', 'password2', value),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextInput);
