import { connect } from 'react-redux';
import { resetUserPassword } from '../../../redux/action_creators/Auth.actioncreator';
import ResetPasswordForm from './ResetPasswordForm.component';

const mapStateToProps = state => ({
  password: state.Inputs.resetPassword.password.value,
});

const mapDispatchToProps = dispatch => ({
  reset: (pass, sessId) => dispatch(
    resetUserPassword(pass, sessId),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResetPasswordForm);
