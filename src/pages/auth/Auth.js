import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import Alert from './alert/Alert.connector';
import LoginCard from './login/LoginCard.connector';
import RegisterCard from './register/Register.connector';

const Auth = ({ location }) => (
  <div id="divPageWrapper" className="auth-overlay">
    <div id="auth-window">
      <Alert />
      <div className="auth-component-container">
        <LoginCard
          returnAdd={queryString.parse(location.search).returnAdd}
        />
        <RegisterCard
          returnAdd={queryString.parse(location.search).returnAdd}
        />
      </div>
    </div>
  </div>
);

Auth.propTypes = {
  location: PropTypes.object.isRequired,
};

export default Auth;
