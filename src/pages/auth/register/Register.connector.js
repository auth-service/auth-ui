import { connect } from 'react-redux';
import {
  createUser,
} from '../../../redux/action_creators/Auth.actioncreator';
import RegisterCard from './Register.component';


const mapStateToProps = state => ({
  email: state.Inputs.registration.email.value,
  username: state.Inputs.registration.username.value,
  password: state.Inputs.registration.password.value,
  password2: state.Inputs.registration.password2.value,
});

const mapDispatchToProps = (dispatch, props) => ({
  register: (login, username, password, password2) => dispatch(
    createUser(login, username, password, password2, props.returnAdd),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterCard);
