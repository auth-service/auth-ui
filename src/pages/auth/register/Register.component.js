import React from 'react';
import PropTypes from 'prop-types';

import Email from './inputs/Email.connector';
import Password from './inputs/Password.connector';
import Username from './inputs/Username.connector';
import Password2 from './inputs/PasswordConfirmation.connector';

const RegisterCard = ({
  register,
  email,
  username,
  password,
  password2,
}) => (
  <div id="register-card" className="auth-card">
    <h3
      className="title is-3 text-align-center"
    >
      Register
    </h3>
    <Email
      id="reg-email"
      className="input text-input"
      placeholder="Email"
      type="text"
    />
    <Username
      id="reg-username"
      className="input text-input"
      placeholder="Username"
      type="text"
    />
    <Password
      id="reg-password"
      className="input text-input"
      placeholder="Password"
      type="password"
    />
    <Password2
      id="reg-password2"
      className="input text-input"
      placeholder="Password Confirmation"
      type="password"
    />
    <p className="align-content-right auth-input">
      <input
        className="button"
        type="button"
        value="Register"
        onClick={() => {
          register(
            email,
            username,
            password,
            password2,
          );
        }}
      />
    </p>
  </div>
);

RegisterCard.propTypes = {
  register: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  password2: PropTypes.string.isRequired,
};


export default RegisterCard;
