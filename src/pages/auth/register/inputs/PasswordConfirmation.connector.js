import { connect } from 'react-redux';
import { updateInput } from '../../../../redux/action_creators/Inputs.actioncreator';
import TextInput from '../../../../assets/components/inputs/text_input/TextInput.component';


const mapStateToProps = state => ({
  value: state.Inputs.registration.password2.value,
  message: state.Inputs.registration.password2.validation.details,
  messageDisplay: state.Inputs.registration.password2.validation.display,
  messageType: state.Inputs.registration.password2.validation.type,
});

const mapDispatchToProps = dispatch => ({
  updateValue: value => dispatch(
    updateInput('registration', 'password2', value),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextInput);
