import { connect } from 'react-redux';
import { updateInput } from '../../../../redux/action_creators/Inputs.actioncreator';
import TextInput from '../../../../assets/components/inputs/text_input/TextInput.component';

const mapStateToProps = state => ({
  value: state.Inputs.registration.password.value,
  messageDisplay: state.Inputs.registration.password.validation.display,
  message: state.Inputs.registration.password.validation.details,
  messageType: state.Inputs.registration.password.validation.type,
});

const mapDispatchToProps = dispatch => ({
  updateValue: value => dispatch(
    updateInput('registration', 'password', value),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextInput);
