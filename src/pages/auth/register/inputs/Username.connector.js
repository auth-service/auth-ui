import { connect } from 'react-redux';
import { updateInput } from '../../../../redux/action_creators/Inputs.actioncreator';
import TextInput from '../../../../assets/components/inputs/text_input/TextInput.component';

const mapStateToProps = state => ({
  value: state.Inputs.registration.username.value,
  messageDisplay: state.Inputs.registration.username.validation.display,
  message: state.Inputs.registration.username.validation.details,
  messageType: state.Inputs.registration.username.validation.type,
});

const mapDispatchToProps = dispatch => ({
  updateValue: value => dispatch(
    updateInput('registration', 'username', value),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextInput);
