import { connect } from 'react-redux';
import { updateInput } from '../../../../redux/action_creators/Inputs.actioncreator';
import TextInput from '../../../../assets/components/inputs/text_input/TextInput.component';

const mapStateToProps = state => ({
  value: state.Inputs.registration.email.value,
  messageDisplay: state.Inputs.registration.email.validation.display,
  message: state.Inputs.registration.email.validation.details,
  messageType: state.Inputs.registration.email.validation.type,
});

const mapDispatchToProps = dispatch => ({
  updateValue: value => dispatch(
    updateInput('registration', 'email', value),
  ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TextInput);
