import React from 'react';
import PropTypes from 'prop-types';

const Authentication = (props) => {
  const {
    display,
    message,
    title,
    close,
  } = props;

  if (display === false) {
    return null;
  }

  return (
    <div className="message is-danger">
      <div className="message-header">
        <p>
          {title}
        </p>
        <button
          className="delete"
          aria-label="delete"
          type="button"
          onMouseUp={() => {
            close();
          }}
        />
      </div>
      <div className="message-body">
        {message}
      </div>
    </div>
  );
};

Authentication.propTypes = {
  display: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
};

export default Authentication;
