import { connect } from 'react-redux';
import { closeErrorMessage } from '../../../redux/action_creators/Auth.actioncreator';

import Alert from './Alert.component';


const mapStateToProps = state => ({
  display: state.Auth.error.display,
  title: state.Auth.error.title,
  message: state.Auth.error.message,
});

const mapDispatchToProps = dispatch => ({
  close: () => dispatch(closeErrorMessage()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Alert);
