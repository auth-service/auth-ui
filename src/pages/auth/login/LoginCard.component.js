import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import TextInput from '../../../assets/components/inputs/text_input/TextInput.component';
import CheckBoxInput from '../../../assets/components/inputs/check_box_input/CheckBoxInput.component';

const LoginCard = ({ login }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(false);

  return (
    <div id="login-card" className="auth-card">
      <h3
        className="title is-3 text-align-center"
      >
        Login
      </h3>
      <TextInput
        id="email-log"
        className="input text-input"
        placeholder="Username or Email"
        type="text"
        value={email}
        updateValue={setEmail}
      />
      <TextInput
        id="password-log"
        className="input text-input"
        placeholder="password"
        type="password"
        value={password}
        updateValue={setPassword}
      />
      <CheckBoxInput
        id="remember-me"
        label="Remember this login"
        value={rememberMe}
        updateValue={setRememberMe}
      />
      <div className="text-input-container">
        <div
          className="text-input"
        >
          <Link to="/forgot-password">
            Forgot Password?
          </Link>
        </div>
      </div>
      <p className="align-content-right auth-input">
        <input
          className="button"
          type="button"
          value="Login"
          onClick={() => {
            login(email, password, rememberMe);
          }}
        />
      </p>
    </div>
  );
};

LoginCard.propTypes = {
  login: PropTypes.func.isRequired,
};


export default LoginCard;
