import { connect } from 'react-redux';
import { login } from '../../../redux/action_creators/Auth.actioncreator';
import LoginCard from './LoginCard.component';


const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch, props) => ({
  login: (
    loginDetail,
    password,
    rememberMe,
  ) => dispatch(login(
    loginDetail,
    password,
    rememberMe,
    props.returnAdd,
  )),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginCard);
