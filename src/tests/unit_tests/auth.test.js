import store from '../../store';

import {
  login,
  // createUser,
  closeErrorMessage,
} from '../../redux/action_creators/Auth.actioncreator';
import { mockFetch } from './helpers/mock.helpers';

import {
  // sucessfulLoginData,
  failedLoginData,
  // unverifiedData,
  // registerUser,
  // failedRegistration,
} from './data/auth.data';

/**
 * Test Sucessful Verified login
 *
 * Expect bearer token set in store
 * Expect bearer token set in local storage
 * Expect user email, id, username set in store
 * Expect error message not to be displayed
 * Expect info banner not to be displayed
 */
// test('should sucessfully log user in', async () => {
//   mockFetch(sucessfulLoginData);
//   await store.dispatch(login('demo', 'demo', true));

//   expect(store.getState().Auth.bearerToken).toEqual('');
//   expect(localStorage.getItem('authData')).toEqual('testToken');
//   expect(store.getState().Auth.userData.email).toEqual('test@test.com');
//   expect(store.getState().Auth.userData.id).toEqual('testId');
//   expect(store.getState().Auth.userData.username).toEqual('test');

//   expect(store.getState().Auth.error.display).toEqual(false);
//   expect(store.getState().Ui.infoBanner.display).toEqual(false);
// });

/**
 * Test Failed login
 *
 * Expect bearer token empty in store
 * Expect bearer token empty in local storage
 * Expect user data to be empty in store
 * Expect error message to be displayed
 */
test('should fail logging user in', async () => {
  mockFetch(failedLoginData);
  await store.dispatch(login('demo', 'demo', true));

  expect(store.getState().Auth.bearerToken).toEqual('');
  expect(localStorage.getItem('bearerToken')).toEqual(null);
  expect(store.getState().Auth.userData).toEqual(null);

  expect(store.getState().Auth.error.display).toEqual(true);
  expect(store.getState().Auth.error.title).toEqual('Test Title');
  expect(store.getState().Auth.error.message).toEqual('Test Message');
});

/**
 * Tests user registration
 *
 * Expect bearer token set in store
 * Expect bearer token set in local storage
 * Expect user email, id, username set in store
 * Expect error message not to be displayed
 * Expect info banner not to be displayed
 */
// test('should create user', async () => {
//   mockFetch(registerUser);
//   await store.dispatch(createUser('demo', 'demo', 'demo'));

//   expect(store.getState().Auth.bearerToken).toEqual('testToken');
//   expect(localStorage.getItem('bearerToken')).toEqual('testToken');
//   expect(store.getState().Auth.userData.email).toEqual('test@test.com');
//   expect(store.getState().Auth.userData.id).toEqual('testId');
//   expect(store.getState().Auth.userData.username).toEqual('test');

//   expect(store.getState().Auth.error.display).toEqual(false);
//   expect(store.getState().Ui.infoBanner.display).toEqual(false);
// });

/**
 * Tests failed registration
 *
 * Expect bearer token empty in store
 * Expect bearer token empty in local storage
 * Expect userdata null in store
 * Expect error message to be displayed
 * Expect info banner not to be displayed
 */
// test('should display failed create user', async () => {
//   mockFetch(failedRegistration);
//   await store.dispatch(createUser('demo', 'demo', 'demo'));

//   expect(store.getState().Auth.bearerToken).toEqual('');
//   expect(localStorage.getItem('bearerToken')).toEqual(null);
//   expect(store.getState().Auth.userData).toEqual(null);

//   expect(store.getState().Auth.error.display).toEqual(true);
//   expect(store.getState().Ui.infoBanner.display).toEqual(false);
// });

/**
 * Test close error message
 *
 * Expect bearer token to be empty in store
 */
test('should close error message', async () => {
  await store.dispatch(closeErrorMessage());

  expect(store.getState().Auth.error.display).toEqual(false);
});
